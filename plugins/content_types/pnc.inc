<?php

$plugin = array(
  'single'          => true,
  'top level'       => true,
  'title'           => t('Node Content'),
  'description'     => t('Displays the Content - Node in the chosen view mode.'),
  'category'        => t('Custom'),
  'weight'          => 1,
  'icon'            => 'kicon.png',
  'edit form'       => 'pnc_content_type_edit_form',
  'admin title'     => 'pnc_content_type_admin_title',
  'admin info'      => 'pnc_content_type_admin_info',
  'render callback' => 'pnc_content_type_render',
  'context'         => new ctools_context_required(t('Node'), 'node'),
  'all contexts'    => true,
  'defaults'        => array(
    'override_title'    => false,
    'use_substitution'  => false,
    'node_id'           => '',
    'view_mode'         => array()
  ),
);


/**
 * 'Edit form' callback for the content type.
 */
function pnc_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['node_add'] = array(
    '#type' => 'markup',
    '#markup' => getNodeTypeList(),
  );
  unset($form['override_title']);
  unset($form['override_title_text']);
  unset($form['override_title_heading']);
  unset($form['override_title_markup']);

  $form['node_id'] = array(
    '#title' => t('Node Title'),
    '#description' => t('Enter the Title of the Node or use a Substitution value like "%node:nid" for actual content if available.'),
    '#type' => 'textfield',
    '#required' => true,
    '#maxlength' => 250,
    '#default_value' => $conf['node_id'],
    '#size'=> 90,
    '#autocomplete_path' => 'node-search/',
  );
  $entity = entity_get_info('node');
  $build_mode_options = array();
  foreach ($entity['view modes'] as $mode => $option) {
    $build_mode_options[$mode] = $option['label'];
  }

  $form['view_mode'] = array(
    '#title' => t('Render Mode'),
    '#type' => 'select',
    '#description' => t('Select how the content should be rendered.'),
    '#options' => $build_mode_options,
    '#default_value' => $conf['view_mode'],
  );

  $form['use_substitution'] = array(
    '#type' => 'checkbox',
    '#default_value' => $conf['use_substitution'],
    '#title' => t('Enable Substitution'),
  );

  $form['contexts'] = array(
    '#title' => t('Substitutions'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $rows = array();
  foreach ($form_state['contexts'] as $context) {
    foreach (ctools_context_get_converters('%' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
      $rows[] = array(
        check_plain($keyword),
        t('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
      );
    }
  }
  $header = array(t('Keyword'), t('Value'));
  $form['contexts']['context'] = array('#markup' => theme('table', array('header' => $header, 'rows' => $rows)));

  return $form;
}

function pnc_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}


function pnc_content_type_admin_title($subtype, $conf, $contexts){
  return t(':: Rendered Node: !node_title ::', array('!node_title' => $conf['node_id']));
}



/**
 * 'admin info' callback for panel pane.
 */
function pnc_content_type_admin_info($subtype, $conf, $context) {
  $outputContent = new stdClass();
  $nid = parseNodeId($conf['node_id'], $context);
  $nodeContent = $nid ? node_view(node_load($nid), $conf['view_mode']) : '';
  $outputContent->title   = t('Preview as !view_mode', array('!view_mode' => $conf['view_mode']));
  $outputContent->content = $nodeContent;
  return $outputContent;
}

function pnc_content_type_render($subtype, $conf, $panel_args, $context = NULL) {
  $nid = parseNodeId($conf['node_id'], $context);
  $nodeContent = ($nid && !empty($conf['view_mode'])) ? node_view(node_load($nid), $conf['view_mode']) : '';
  $renderedNode = drupal_render($nodeContent);
  $noHtmlNode = trim( str_replace( PHP_EOL, '',  strip_tags($renderedNode, '<img><h1><h2><h3><h4><h5><h6><p><a><strong><b>')) );
  $outputContent = new stdClass();
  $outputContent->content = (!empty($noHtmlNode)) ? $renderedNode : '';
  return $outputContent;
}

function getNodeTypeList() {
  $nodeTypes = node_type_get_types();
  $returnStr = '';
  $exclusionTypes = array('panel', 'webform');
  foreach(array_keys($nodeTypes) as $nodeType) {
    if(!in_array($nodeType, $exclusionTypes)) {
      $returnStr .= ' | <span>:: ' . l(ucfirst($nodeType), '/node/add/'. $nodeType , array('attributes' => array('target' => '_blank'))).' ::</span> '  . PHP_EOL;
    }
  }
  return '<div style="float:left; width: 40%;"><hr /> <h3>Add new Content before: </h3>' . $returnStr . '|<br /><hr /><br /></div>';
}


function parseNodeId($rawId, $context) {
  $nid = ctools_context_keyword_substitute($rawId, array(), $context);
  if(!$nid || !is_numeric($nid)) {
    preg_match('/(.*\[nid\:$)?(\d+)(\]$)?/', $rawId, $matchId);
    $nid = isset($matchId['2']) && is_numeric($matchId['2']) ? $matchId['2'] : false;
  }

  return $nid;
}